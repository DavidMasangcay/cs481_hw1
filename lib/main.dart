//*********************
//David Masangcay
//CS 481 - HW1
//*********************
import 'package:flutter/material.dart';

void main()
{
  runApp(MyApp());
}

class MyApp extends StatelessWidget
{
  @override

  Widget build(BuildContext context)
  {
    return MaterialApp(
        title: 'Flutter Hello Word Demo',
        home: Scaffold(
            appBar: AppBar(
                title: Text("The Blue App Bar, Nice")
            ),

            body: Center(
              child: Text("Hello World - David Masangcay")
            )
        )
        );
  }
}

